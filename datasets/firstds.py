#Jeff- The xlsx had to be changed to xls because of a strict file format issue. Hopefully this is ok. 
import pandas as pd
data = pd.read_excel("APtestds.xls")
df = pd.DataFrame(data)
print(df)

print(df.info())

df.index.name = "user_id"
print(df)
print(df.loc[32], "Unnamed: 1")
print(df.columns)
df.columns.name = "Properties"

df.rename(columns={"Unnamed: 1": "Schools", "Unnamed: 2": "9th grade"})
print(df)
