# Where did the data come from?
- This data came from the [CollegeBoard website](https://reports.collegeboard.org/ap-program-results/data-archive).
- The CollegeBoard must have gotten this data from past AP test results. 
# How was it gathered? Why?
- This data was gathered through the analysis of AP test results. For example, 
they looked at the amout of girls and boys from each grade and looked at how 
many total students took each test. They also have another dataset of the scores
(1, 2, 3, 4, or 5). 
- This data set was mot likely gathered for workers of the CollegeBoard to study
test results,teachers, or curious students like me and Jamethiel.  
# Who owns the data? With what usage rights is it being made available?
- I belive the CollegeBoard owns this data.
- I have not seen any usage rights but on the site it says, "We’re streamlining 
how we report data on College Board programs and services, including AP data.
Here you can download extensive reports about AP Exam administrations dating back to 2002." This makes me assume that you can download it and use it how you
like.
