import pandas as pd

data = pd.read_excel("APtestds.xlsx")

df = pd.DataFrame(data)
print(df)
print(df.info())
print(df.dtypes)
print(df.index)

df.index.name = "Index"
print(df)

print(df.reset_index())
print(df.reset_index().set_index("Class"))

print(df.sort_values(["Class"]))

print(df.columns)

df.columns.name = "properties"
print(df)

print(df.rename(columns={"Class": "AP Class"}))

print(df.drop(columns=["Class"]))

print(df.T) # Shortcut for df.transpose()
# columns into rows and vice versa

print(df.loc[:, ["2019 PROGRAM TOTAL", "2020 PROGRAM TOTAL", "Class", "% CHANGE 2019-2020", "SCHOOLS"]])
print(df.loc[10, "Class"])
print(df.loc[[11, 12], "Class"])
print(df.loc[:10, ["Class", "9TH GRADE"]])
print(df["Class"])

print(df.iloc[1, 2]) # index 1 and column 2
print(df.iloc[:, [2]]) # all rows of column 2
print(df.iloc[[1], :]) # index 1 all columns

tf = (df["% CHANGE 2019-2020"] > 0) & (df["9TH GRADE"] > 400)
print(tf)

print(df.loc[df.index > 20, :])
print(df.loc[df["Class"].isin(["MUSIC THEORY", "PSYCHOLOGY", "COMPUTER SCIENCE PRINCIPLES", "MACROECONOMICS", "MICROECONOMICS"]), :])

df_multi = df.reset_index().set_index(["Class", "SCHOOLS"])
df_multi = df_multi.sort_index()
print(df_multi)
print(df_multi.reset_index(level=0))

df2 = df.copy()
df2.loc[[1, 4], "Class"] = ["Art", "Bio"]
print(df2)

tf = (df2["SCHOOLS"] < 5000) | (df2["9TH GRADE"] < 600)
df2.loc[tf, "Class"] = "xxx"
print(df2)

print(df2.replace("CALCULUS AB", "Calc AB"))

print(df2.dropna())

print(df2.fillna({"SCHOOLS": df2["SCHOOLS"].mean()}))

print(df["Class"].is_unique)

df2_cleaned = df2.loc[:, "Class"].str.strip()
print(df2_cleaned)

print(df.mean(axis=0))
print(df.groupby(["SCHOOLS"]).mean())
pivot = pd.pivot_table(data,
        index="SCHOOLS", columns="Class",
        values="2020 PROGRAM TOTAL", aggfunc="sum",
        margins=True, margins_name="2020 TOTAL")
print(pivot)
print(pd.melt(pivot.iloc[:-1,:-1].reset_index(),
        id_vars="SCHOOLS",
        value_vars=["ART AND DESIGN: DRAWING","ART HISTORY","BIOLOGY","CALCULUS AB","CALCULUS BC","CHEMISTRY","CHINESE LANGUAGE & CULTURE","COMPUTER SCIENCE A","COMPUTER SCIENCE PRINCIPLES","MACROECONOMICS","MICROECONOMICS","ENGLISH LANGUAGE & COMP.","ENGLISH LITERATURE & COMP.","ENVIRONMENTAL SCIENCE","EUROPEAN HISTORY","FRENCH LANGUAGE & CULTURE","GERMAN LANGUAGE & CULTURE","GOVT. & POL. - COMP.","GOVT. & POL. - U.S.","HUMAN GEOGRAPHY","ITALIAN LANGUAGE & CULTURE","JAPANESE LANGUAGE & CULTURE","LATIN ","MUSIC THEORY","PHYSICS C - E&M","PHYSICS C - MECH","PHYSICS 1","PHYSICS 2","RESEARCH","SEMINAR","SPANISH LANGUAGE & CULTURE","SPANISH LITERATURE & CULTURE","STATISTICS","U.S. HISTORY","WORLD HISTORY"],
        value_name="2020 PROGRAM TOTAL"))
