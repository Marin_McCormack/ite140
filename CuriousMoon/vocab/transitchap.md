# Chapter Two (Transit)
# New Vocab words:
- PG Admin IV
- Navicat
- SQLPro
- Postico
- Symlnked
- Extract, Transform, Load(ETL)

# Skills That Might Require Additional Investigation:
- postgres client psql
- /bin directory of postgres
- which psql
- createdb
- dropdb
- making a table
- drop if exists 
- nextval
- alter
