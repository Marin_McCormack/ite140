# About Amazon Snowball
## How it's used in the text?
"'I would have thought we would have all of this on ==Amazon Snowball==,
Google BigTable or... some other cloud service,' I say, curious."(A Curious
Moon pg.3)
## Description:
AWS says that "Snowball is a [petabyte-scale](https://en.wikipedia.org/wiki/Byte#Multiple-byte_units) data transport solution that
uses secure appliances to transfer large amounts of data into and out of the
AWS cloud. Using Snowball addresses common challenges with large-scale data 
transfers including high network costs, long transfer times, and security
concerns."
## My understaning:
Amazon snowball is a service that provides secure, rugged devices, so you
can bring AWS computing and storage capabilities to your edge environments, 
and transfer data into and out of AWS. 
## Why is it used?
The AWS Snowball service uses physical storage devices to transfer large 
amounts of data between Amazon Simple Storage Service [Amazon S3](https://en.wikipedia.org/wiki/Amazon_S3) and your
onsite data storage location at faster-than-internet speeds.
