# Week 11
## For week 11 Jamethiel, Aaron and I (Marin) worked with `cdevlin`!
`cdevlin` is a database server that was interesting to work with. To get it running we turned it on and waited for the command line to pop up on the monitor. Once that happened we had to sudo apt intall

Jamethiel and I added ourselves as super users using the `adduser` command. After we did that we were then able to add Aaron & Jeff as super users too. 

Jamethiel and I set up a Raspberry Pi database server in class for ourselves where at home we can have sudo permissions. First, we had to take the SD card out ot the Raspberry Pi and put it into the computer on the side. Then, we opened Raspberry Pi Imager to flash it with the OS image. Lastly, we created our database server and were able to ssh into it by useing the command: 
`ssh mmccormack@marin.local` (example)
