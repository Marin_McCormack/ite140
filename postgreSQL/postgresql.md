# PostgreSQL Tutorial notes
## What is postgeSQL?

PostgreSQL is a powerful, open source object-relational database system that extends the SQL language combined with many features that safely store and scale the most complicated data workloads.

- Runs on all major operating systems
- Powerful add-ons such as the popular PostGIS (adds support for geographic objects)
- Highly extensible. For example, you can define your own data 
  types, build out custom functions, even write code from different programming 
  languages without recompiling your database
- ACID-compliant since 2001

## ACID

**A - Atonomy**  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;All or nothing transactions 

**C - Consistency**  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Guarentees committed transaction state

**I - Isolation**  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Transactions are indepemdant 

**D - Durability**  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Commited data is never lost

## Origins

PostgreSQL dates back to 1986 as part of the POSTGRES project at the University of California at Berkeley and has more than 35 years of active development on the core platform. It was created by a computer science professor Michael Stonebraker and his team. 

In 1994, Andrew Yu and Jolly Chen added an SQL language interpreter to POSTGRES. Under a new name, Postgres95 was subsequently released to the web to find its own way in the world as an open-source descendant of the original POSTGRES Berkeley code.

By 1996, it became clear that the name “Postgres95” would not stand the test of time. A new name was chosen PostgreSQL, to reflect the relationship between the original POSTGRES and the more recent versions with SQL capability.  

## Key Features
- Helps developers to build applications.
- It allows administrators to build fault-tolerant environment by protecting data integrity.
- Compatible with various platforms using all major languages and middleware.
- It offers a most sophisticated locking mechanism.
- Support for multi-version concurrency control.
- Mature Server-Side Programming Functionality.
- Compliant with the ANSI SQL standard.
- Full support for client-server network architecture.
- Log-based and trigger-based replication SSL.
- Standby server and high availability.
- Object-oriented and ANSI-SQL2008 compatible.
- Support for JSON allows linking with other data stores like NoSQL which act as a federated hub for polyglot databases.

## Applications of postgreSQL
- Financial
- Government GIS data
- Manufacturing 
- Web technology and NoSQL
- Scientific data

# PostgreSQL Data Types: Byte, Numeric, Character, Binary
## Character Datatypes


| Name | Description | 
| -------- | -------- | 
| varchar(n)|Allows you to declare variable-length with a limit     |
|Char(n) | Fixed-length, blank padded|
|Text|Use can use this data type to declare a variable with unlimited length|

## Numeric Datatypes
- Integers 
- Floating-Point Numbers


| Name | Store size | Range |
| -------- | -------- | -------- |
smallint|	2 bytes|	-32768 to +32767
integer	|4 bytes	|-2147483648 to +2147483647
bigint	|8 bytes|	-9223372036854775808 to 9223372036854775807
decimal	|variable|	If you declared it as decimal datatype ranges from 131072 digits before the decimal point to 16383 digits after the decimal point
numeric|	variable|	If you declare it as the number, you can include number up to 131072 digits before the decimal point to 16383 digits after the decimal point
real	|4 bytes|	6 decimal digits precision
double	|8 bytes|	15 decimal digits precision
