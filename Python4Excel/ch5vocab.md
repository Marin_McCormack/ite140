## [Data Sets](https://en.wikipedia.org/wiki/Data_set) (My vocab word)
- A data set is a collection of data. For each member of the data set lists 
values for each of the variables, for example height and weight of an object.
Data sets can also have a collection of files or documnets. 
- Characteristics that define a data sets structure and properties:
    - Number and type of attributes or variables.
    - [Statistical measures](https://en.wikipedia.org/wiki/Statistical_parameter) example standard deviation and [kurtosis](https://en.wikipedia.org/wiki/Kurtosis).
- You can have a data set with numerical values or without numerical values 
example: Ethnicity of a person would be a data set without numerical values in 
it. 
- For each variable in the data set the values are normally the same kind, but
you can also have "missing values" which must be designated in some way.
### Where do data sets come from?
- Data sets usually come from observations aquired by sampling a statistical 
population. Each row of the data set correlates to the observations on one 
element of that population. 
- Data sets can be created by algorithms with the objective of cirtian kinds of 
software 
## [Jupyter notebooks](https://en.wikipedia.org/wiki/Project_Jupyter#Jupyter_Notebook)
- A web-based interactive computational environment for producing notebook 
documents. Made using sveral open-source librarys. 
## [Visualization](https://en.wikipedia.org/wiki/Visualization_(graphics))
- Visualization is the use of interactive, sensory representations, typically 
visual, of abstract data to reinforce cognition, hypothesis building, and 
reasoning.
## [Pandas](https://en.wikipedia.org/wiki/Pandas_(software))
- A software library writen for the python programming language used for data
manipulation & analysis. It offers data structures and operations for
manipulating numerical tables and time series. 
## [Interface](https://en.wikipedia.org/wiki/Interface_(computing))
- an interface is a shared boundary across which two or more separate components
of a computer system exchange information. The exchange can be between software,
computer hardware, peripheral devices, humans, and combinations of these.
## [Statistics](https://en.wikipedia.org/wiki/Statistics)
- Statistics is the discipline that concerns the collection, organization,
analysis, interpretation, and presentation of data.
## [Time Series](https://en.wikipedia.org/wiki/Time_series_database)
- A software system that is optimized for storing and serving time series 
through associated pairs of time(s) and value(s). In some fields, time series 
may be called profiles, curves, traces or trends.
## [Vectorization](https://en.wikipedia.org/wiki/Array_programming)
- Utilizing a CPU's vector-based instructions if it has them or by using
multiple CPU cores. 
## [Data Alignment](https://en.wikipedia.org/wiki/Data_structure_alignment)
- The aligning of elements according to their natural alignment.
## [cleaning data](https://en.wikipedia.org/wiki/Data_cleansing)
- The process of noticing and correcting corrupt or inaccurate records from a
record set, table, or database and refers to identifying incomplete, incorrect, 
inaccurate or irrelevant parts of the data and then replacing, modifying, or
deleting the dirty or coarse data.
## [aggregation](https://en.wikipedia.org/wiki/Aggregate_function)
- A function where the values of multiple rows are grouped together to form one
summary value.
## [descriptive statistics](https://en.wikipedia.org/wiki/Descriptive_statistics)
- The process of using and analysing summaries of features from a collection of 
information.
