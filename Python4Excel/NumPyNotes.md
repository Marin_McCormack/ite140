# Python For Excel Chapter 4 Notes
## What is [NumPy](https://en.wikipedia.org/wiki/NumPy)?
- Python for Excel says that "NumPy is the core package for scientific computing in Python, providing support for array-based calculations and linear algebra."
- In a NumPy array all of the elements need to be the same data type.
- The NumPy data type is float64

## Vectotization and Broadcasting
- Vectorization: NumPy will perform an element- wise operation, which means that
 you don’t have to loop through the elements your‐ self.
- Scalar: a basic Python data type like a float or a string.
- Broadcasting: When NumPy extends the smaller arrays automatically across the 
larger array so that their shapes become compatible. 

## Creating and Manipulating Arrays
- Chained indexing: Example matrix[0][0] will get you the first element of the
first row.
### Useful Array Constructors
- Arange Function: Stands for array range and it returns a NumPy array.

 


