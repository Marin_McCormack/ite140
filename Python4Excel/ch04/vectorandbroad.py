#This file is about an example with vectorization and broadcasting
import numpy as np
array1 = np.array([[1., 2., 3.],
                    [4., 5., 6.]])
print(array1)
multadd = input('Would you like to multiply or add a value to each value in array 1?\n') 
if multadd == "add":
    add = int(input("What value would you like to add to every number in array 1?\n"))
    print(array1 + add)
if multadd == "multiply":
   mult =  int(input("What value would you like to multiply to every number in array 1?\n"))
   print(array1 * mult)

