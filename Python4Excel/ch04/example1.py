import numpy as np
import math
array1 = np.array([10, 100, 1000.])

array2 = np.array ([[1., 2., 3.],
                    [4., 5., 6.]])

print(np.array([[math.sqrt(i)for i in row]for row in array2]))

print(array2.sum(axis=0))
print(array2.sum())


print(array1.dtype)
print(array2 + 1)
print(array2 * array2)
print(array2 * array1)
print(array2 @ array2.T)

print(array2[1, 1])

print(np.arange(2 * 5).reshape(2, 5))
print(np.random.randn(2,3))

