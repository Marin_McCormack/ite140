import pandas as pd
data = [["Mark", 55, "Italy", 4.5, "Europe"],
        ["John", 33, "USA", 6.7, "America"],
        ["Tim", 41, "USA", 3.9, "America"],
        ["Jenny", 12, "Germany", 9.0, "Europe"]]
#The part on the left of the equal sign is the name of the function parameter 
#the part on the right of the equal sign is the name of the variable that you pass into the function as argument. 
df = pd.DataFrame(data=data,
                  columns=["name", "age", "country",
                           "score", "continent"],
                  index=[1001, 1000, 1002, 1003])
print(df)
print(df.info())

## Working with indices
print(df.index)

df.index.name = "user_id"
print(df)
df.reset_index()

print(df.reset_index().set_index("name"))

print(df.reindex([999, 1000, 1001, 1004]))

print(df.sort_values(["continent", "age"]))
print("\n")
## Working with coulmns


print(df.columns)
print("\n")
df.columns.name = "properties"
print(df)
print("\n")
## To rename columns:
print(df.rename(columns={"name": "First Name", "age": "Age"}))
print("\n")
print(df.drop(columns=["name", "country"],
        index=[1000, 1003]))
print("\n")
print(df.T)## Short for df.transpose()

print("\n")
## Using loc
print(df.loc[:, ["continent", "country", "name", "age", "score"]])
## Working with loc
## loc stands for location
## General format: df.loc[row_selection, column_selection]
print("\n")

print(df.loc[1001, "name"])

print("\n")

print(df.loc[[1001, 1002], "age"])

print("\n")

print(df.loc[:1002, ["name", "country"]])

## Diference between DataFrame and series: Even with a single column, DataFrames are two- dimensional, while Series are one-dimensional. Both DataFrame and Series have an index, but only the DataFrame has column headers. When you select a column as Series, the column header becomes the name of the Series. 

